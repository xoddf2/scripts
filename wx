#!/bin/sh

# This is a simple script that displays current weather conditions and a zone
# forecast or the latest radar image from the National Weather Service.
#
# Usage: Run without arguments for the current conditions and zone forecast, or
# run with the -r flag to display radar.
#
# Configuration:
# To change the observations site, forecast zone, and radar site, change the
# $cc, $zone, and $radar variables, respectively.
#
# For a list of observations sites, see:
# https://tgftp.nws.noaa.gov/data/observations/metar/decoded/
#
# For a list of forecast zones, see:
# https://tgftp.nws.noaa.gov/data/forecasts/zone/
# The $zone variable follows the format "<state>/<zone ID>".
#
# For a list of radar sites, see:
# https://radar.weather.gov/ridge/lite/
#
# Dependencies: curl(1), display(1) (from ImageMagick)

cc="KRDD"
zone="ca/caz015"
radar="KBBX"

if [ "$1" = "-r" ]; then
	display "https://radar.weather.gov/ridge/standard/${radar}_0.gif"
else
	echo "********** Current Conditions **********"
	curl -s "https://tgftp.nws.noaa.gov/data/observations/metar/decoded/${cc}.TXT"
	echo
	echo "********** NWS Zone Forecast **********"
	curl -s "https://tgftp.nws.noaa.gov/data/forecasts/zone/${zone}.txt"
fi
